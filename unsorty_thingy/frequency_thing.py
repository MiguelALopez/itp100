freqs = {} 
alice = open('alice_in_wonderland.txt', 'r')
for line in alice:
    for char in line:
        if char in freqs:
            freqs[char] += 1
        else:
            freqs[char] = 1

print(freqs)
