from gasp import *
begin_graphics()
finished = False
player_x = 5
player_y = 5
def player_shape():
    c = Circle((player_x, player_y), 5, filled=True)
    return c
def move_player(player_shape: Circle):
    global player_x, player_y
    key = update_when('key_pressed')
    if key == 'd' and player_x < 63:
        player_x += 1
    if key == 'a' and player_x < 63:
        player_x -= 1
    if key == 'w' and player_y > 0:
        player_y += 1
    if key == 's' and player_y > 0:
        player_y -= 1
    if key == 'x':
        finished = not finished
        return
    move_to(player_shape, (10*player_x+5, 10*player_y+5))

ps = player_shape()

while not finished:
    move_player(ps)

end_graphics()

