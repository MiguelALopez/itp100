from gasp import *
begin_graphics()
finished = False
poo = 15
player_x = random_between(0, 63) * poo 
player_y = random_between(0, 47) * poo
robot_x = random_between(0, 63) * poo
robot_y = random_between(0, 47) * poo
c = Circle((player_x, player_y), 5, filled=True)
r = Circle((robot_x, robot_y), 10, filled=False)

while not finished:
    
    key = update_when('key_pressed')
    if key == 'up': 
        player_y += poo
    elif key == 'down':
        player_y -= poo
    elif key == 'right':
        player_x += poo
    elif key == 'left':
        player_x -= poo
    move_to(c, (player_x, player_y))
    
    coin = random_between(0, 1)
    
    if coin == 1 and player_y != robot_y:
        if robot_y < player_y:
            robot_y += poo
        elif robot_y > player_y:
            robot_y -= poo
    
    elif coin == 0 and player_x != robot_x:
        if robot_x < player_x:
            robot_x += poo
        elif robot_x > player_x:
            robot_x -= poo
    move_to(r,(robot_x, robot_y))
    
    player_x = player_y % 640
    player_y %= 480
    robot_x %= 640
    robot_y %= 480

    if robot_x == player_x and robot_y == player_y:
        finished == True

while True:
    key = update_when('key_pressed')
    if key == 'x':
        break
end_graphics()
