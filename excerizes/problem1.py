def only_evens(nums):
        """
            >>> only_evens([3, 8, 5, 4, 12, 7, 2])
                [8, 4, 12, 2]
            >>> my_nums = [4, 7, 19, 22, 42]
            >>> only_evens(my_nums)
                [4, 22, 42]
            >>> my_nums
                [4, 7, 19, 22, 42]
        """
