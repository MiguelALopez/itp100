def average(nums):
    """
    Return the average number of a list of numbers
    >>> average([2, 4])
    3.0
    >>> average([2, 6])
    4.0
    """

######################

def only_evens(nums):
    """
    >>> only_evens([3, 8, 5, 4, 12, 7, 2])
        [8, 4, 12, 2]
  i  >>> my_nums = [4, 7, 19, 22, 42]
    >>> only_evens(my_nums)
        [4, 22, 42]
    >>> my_nums
        [4, 7, 19,  22, 42]
    """
    list = []
    for i in only_evens:
        if i % 2 == 0:
            list.append(i)
    return list

#######################
def num_even_digits(n):
    """
    >>> num_even_digits(123456)
    3
    >>> num_even_digits(2468)
    4
    >>> num_even_digits(1357)
    0
    >>> num_even_digits(2)
    1
    >>> num_even_digits(20)
    2
    """
    list = 0
    for i in str(n):
        if int(i) % 2 == 0:
            list += 1
        return list
######################
def sum_of_square_of_digits(n):
    """
    >>> sum_of_squares_of_digits(1)
    1
    >>> sum_of_squares_of_digits(9)
    81
    >>> sum_of_squares_of_digits(11)
    2
    >>> sum_of_squares_of_digits(121)
    6
    >>> sum_of_squares_of_digits(987)
    197
    """
    list = 0
    for i in str(n):
        list += int(i) ** 2
    return list
########################
def lots_of_letters(word):
    """
    >>> lots_of_letters('Lidia')
    'Liidddiiiiaaaaaa'
    >>> lots_of_letters('Python')
    'Pyyttttthhhhoooonnnnnnn'
    >>> lots_of_letters('')
    ''
    >>> lots_of_letters('1')
    '1'
    """
    list = ''
    for i in range(len(word)):
        p = i + 1
        list += word[i] * p
    return list
########################
def gcf(m, n):
    """
    >>> gcf(10, 25)
    5
    >>> gcf(8, 12)
    4
    >>> gcf(5, 12)
    1
    >>> gcf(24, 12)
    12
    """
if __name__ == '__main__':
    import doctest
    doctest.testmod()
