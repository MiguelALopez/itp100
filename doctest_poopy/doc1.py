def to_binary(n):
    """
    Convert an decimal integer into a string representation of its binary
    (base2) digits.

      >>> to_binary(10) 
      '1010'
      >>> to_binary(12) 
      '1100'
      >>> to_binary(0) 
      '0'
      >>> to_binary(1) 
      '1'
    """
    if n == 0:
        return '0'
    if n > 1:
        to_binary(n//2)
    print(n % 2, end='')
if __name__ == '__main__':
    import doctest
    doctest.testmod()
