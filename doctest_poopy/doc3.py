def to_base4(n):
    """
    Convert an decimal integer into a string representation of its base4
    digits.

      >>> to_base4(20)
      '110'
      >>> to_base4(28)
      '130'
      >>> to_base4(3)
      '3'
    """
    if n == 0:
        return [0]
    digits = []
    if n > 1:
        to_base4(n//4)
    print(n % 4, end='')
if __name__ == '__main__':
    import doctest
    doctest.testmod()

