def to_base(numba, base):
    """
       >>> to_base(10, 3)
       '101'
       >>> to_base(11, 2)
       '1011'
       >>> to_base(10, 6)
       '14'
       >>> to_base(21, 3)
       '210'
       >>> to_base(21, 11)
       '1A'
       >>> to_base(47, 16)
       '2F'
       >>> to_base(65535, 16)
       'FFFF'
       >>> to_base(69, 64)
       'BF'
       >>> to_base(420, 64)
       'Gk'
       >>> to_base(21, 64)
       'V'
       >>> to_base(8008135, 64)
       'ejHH'
     """
    digits = "0123456789ABCDEF"
    base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    numstr = ''
    while numba:
        if base == 64:
            digits = base64
        numstr = digits[numba % base] + numstr
        numba //= base
    return numstr


if __name__ == '__main__':
    import doctest
    doctest.testmod()
