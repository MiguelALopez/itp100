name = "Mark"
start = 'My name is '
combined = start + name
print(len(combined))
print(combined)
print(name * 3)

a_string = "fart"
b_string = "bow"
c_string = (a_string + b_string) * 2
print(len(c_string))

my_first_list = [12, "ape", 13]
print(len(my_first_list))
print(my_first_list * 3)
my_second_list = my_first_list + [321.4]
print(my_second_list)

def name_procedure(name1):
    start1 = "My name is"
    combined = start1 + (name1 * 2)
print(combined)
print(len(combined))

name_procedure("John")

def item_lister(items):
    items[0] = "First item"
    items[1] = items[0]
    items[2] = items[2] , items[1]
    print(items)

item_lister([2, 4, 6, 8])

def grade_average(a_list):
    sum = 0
    for num in a_list:
        average = (99 + 100 + 74 + 63 + 100 + 100) / 6
    return average

a_list = [99, 100, 74, 63, 100, 100]
print(grade_average(a_list))

source = ["This", "is", "a", "list"]
so_far = []
for index in range(0, len(source)):
    so_far = so_far + [source[index]]
    print(so_far)

items = ["hi", 2, 3, 4]
items[0] = items[0] + items[0]
items[1] = items[3] - items[3]
items[2] = items[1]
print(items)

source = ["This","is", "a","list"]
so_far = []
for index in range(0, len(source)):
    so_far = [source[index]] + so_far
    print(so_far)

source = ["This","is", "a","list"]
so_far = []
for index in range(0, len(source)):
    so_far = so_far + [source[index]] + so_far
    print(so_far)

numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
even_list = []
for index in range(0,len(numbers),2):
    even_list = even_list + [numbers[index]]
print(even_list)

numbers = range(0, 50)
even_list = []
for index in range(0,len(numbers),2):
    even_list = even_list + [numbers[index]]
print(even_list)

for index in range(0, 6):
     print(index)

source = ["This", "is", "a", "list"]
new_list = []
for index in range(len(source)):
    new_list = new_list + [source[index]]
print(new_list)

numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
odd_list = []
for index in range(1,len(numbers),2):
    odd_list = odd_list + [numbers[index]]
print(odd_list)


numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
odd_list = []
for index in range(0,len(numbers),5):
    odd_list = odd_list + [numbers[index]]
print(odd_list)


